package id.chalathadoa.challengechapter4

import android.content.Context
import android.content.SharedPreferences

class LoginManager(var context: Context?) {

    val PRIVATE_MODE = 0
    private val SHARPREF = "SharedPreferences"
    private val IS_LOGIN = "is_login"

    var pref: SharedPreferences? = context?.getSharedPreferences(SHARPREF, PRIVATE_MODE)
    var editor: SharedPreferences.Editor? = pref?.edit()

    fun setLogin(isLogin: Boolean){
        editor?.putBoolean(IS_LOGIN, isLogin)
        editor?.commit()
    }

    fun setUsername(username: String?){
        editor?.putString("username", username)
        editor?.commit()
    }

    fun isLogin(): Boolean? {
        return pref?.getBoolean(IS_LOGIN, false)
    }

    fun getUsername(): String? {
        return pref?.getString("username", "")
    }

    fun removeData(){
        editor?.clear()
        editor?.commit()
    }
}