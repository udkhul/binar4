package id.chalathadoa.challengechapter4

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.chalathadoa.challengechapter4.adapter.MyNoteAdapter
import id.chalathadoa.challengechapter4.database.MyNoteDatabase
import id.chalathadoa.challengechapter4.database.Note
import id.chalathadoa.challengechapter4.database.User
import id.chalathadoa.challengechapter4.databinding.FragmentHomeBinding
import id.chalathadoa.challengechapter4.helper.MyNoteRepo
import id.chalathadoa.challengechapter4.helper.SharedPref
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FragmentHome : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPref
    private lateinit var myNoteRepo: MyNoteRepo
    private lateinit var user: User
    private lateinit var myNoteAdapter: MyNoteAdapter
    private var mDb: MyNoteDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDb = MyNoteDatabase.getInstance(requireContext())
        myNoteRepo = MyNoteRepo(requireContext())
        welcomeUser()
        btnLogout()
    }

    private fun initRecyclerView(){
        binding.apply {
            myNoteAdapter = MyNoteAdapter({}, {}, action)
            rvData.adapter = myNoteAdapter
            rvData.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun btnLogout() {
        //val clear = userPref.clearUser()
        binding.btnLogout.setOnClickListener {
            sharedPref.clearUser()
            findNavController().navigate(R.id.action_fragmentHome_to_fragmentLogin)
        }
    }
    private fun welcomeUser(){
        binding.tvWelcomeHome.setText("Welcome, ${user.username}")
    }

    /**
     * floating add button
     */

    private fun addNote(){
        binding.fabAdd.setOnClickListener {
            showAlertDialog(null, true)
        }
    }

    private fun showAlertDialog(note: Note?, isFromButtonAdd: Boolean) {
        val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.layout_show_dialog, null, false)

        val etTitle = customLayout.findViewById<EditText>(R.id.et_title)
        val etBody = customLayout.findViewById<EditText>(R.id.et_body)
        val btnSave = customLayout.findViewById<Button>(R.id.btn_save)

        if (!isFromButtonAdd && note != null){
            etTitle.setText(note.title)
            etBody.setText(note.body)
            btnSave.text = "Update"
        }

        val builder = AlertDialog.Builder(requireContext())
        builder.setView(customLayout)
        val dialog = builder.create()

        btnSave.setOnClickListener {
            val title = etTitle.text.toString()
            val body = etBody.text.toString()

            if (note != null){
                val newNote = Note(note.noteId, userEmail = user.userEmail, title, body)
                updateToDb(newNote)
            } else {
                saveToDb(title, body)
            }
            dialog.dismiss()
        }
        dialog.show()
    }

    fun showAlertDialogAdd(){
        createCustomDialog { title, body ->
            saveToDb(title, body)
        }
    }

    private fun showAlertDialogUpdate(note: Note){
        createCustomDialog { title, body ->
            val newNote = Note(note.noteId, userEmail = user.userEmail, title, body)
            updateToDb(newNote)
        }
    }

    private fun createCustomDialog(onClickListener: (String, String) -> Unit){
        val customLayout = LayoutInflater.from(requireContext()).inflate(androidx.core.R.layout.custom_dialog, null, false)

        val etTitle = customLayout.findViewById<EditText>(R.id.et_title)
        val etBody = customLayout.findViewById<EditText>(R.id.et_body)
        val btnSave = customLayout.findViewById<Button>(R.id.btn_save)

        val builder = AlertDialog.Builder(requireContext())
        builder.setView(customLayout)

        val dialog = builder.create()

        btnSave.setOnClickListener {
            onClickListener.invoke(etTitle.text.toString(), etBody.text.toString())
        }
        dialog.show()
    }

    private fun saveToDb(title: String, body: String){
        val note = Note(null, userEmail = user.userEmail, title, body)
        CoroutineScope(Dispatchers.IO).launch {
            val result = note?.let { mDb?.myNoteDao()?.insertNote(it) }
            if (result != 0L){
                getDataFromDb()
                showToastInMainThread("Catatan berhasil ditambahkan!")
            } else {
                showToastInMainThread("Gagal Ditambahkan")
            }
        }
    }

    private fun showToastInMainThread(message: String){
        CoroutineScope(Dispatchers.Main).launch {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun getDataFromDb(){
        CoroutineScope(Dispatchers.IO).launch {
            val result2 = myNoteRepo.getData()
            if (result2 != null){
                CoroutineScope(Dispatchers.IO).launch {
                    myNoteAdapter.updateData(result2)
                }
            }
        }
    }

    private fun updateToDb(note: Note){
        CoroutineScope(Dispatchers.IO).launch {
            var result = myNoteRepo.updateNote(note)
            if (result != 0) {
                getDataFromDb()
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Catatan Berhasil Diupdate", Toast.LENGTH_SHORT).show()
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Catatan Gagal diupdate", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun deleteItemDb(note: Note){
        CoroutineScope(Dispatchers.IO).launch {
            val result = myNoteRepo.deleteNote(note)
            if (result != 0){
                getDataFromDb()
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(requireContext(), "Catatan Berhasil dihapus", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(requireContext(), "Catatan Gagal dihapus", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val action = object: MyNoteAdapter.MyNoteActionListener {
        override fun onDelete(note: Note) {
            deleteItemDb(note)
        }

        override fun onEdit (note: Note){
            showAlertDialog(note, false)
        }
    }
}