package id.chalathadoa.challengechapter4

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.findNavController
import id.chalathadoa.challengechapter4.database.User
import id.chalathadoa.challengechapter4.databinding.FragmentRegisterBinding
import id.chalathadoa.challengechapter4.helper.MyNoteRepo
import id.chalathadoa.challengechapter4.helper.SharedPref
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FragmentRegister : Fragment() {

    private var _binding : FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPref

    private lateinit var user: User
    private lateinit var myNoteRepo: MyNoteRepo

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkConfirmPasswordOnChange()
        btnRegister()
        btnToLogin()
    }

    private fun checkConfirmPsw(): Boolean{
        binding.apply {
            return etMasukkanPasswordRegister.text.toString() == etKonfirmasiPasswordRegister.text.toString()
        }
    }

    private val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun afterTextChanged(p0: Editable?) {
            if (checkConfirmPsw() || p0.isNullOrEmpty()){
                binding.etKonfirmasiPasswordRegister.error = null
            } else {
                binding.etKonfirmasiPasswordRegister.error = "password tidak sesuai"
            }
        }
    }

    private fun checkConfirmPasswordOnChange(){
        binding.etKonfirmasiPasswordRegister.addTextChangedListener(textWatcher)
        binding.etKonfirmasiPasswordRegister.doAfterTextChanged {  }
    }

    private fun btnRegister(){
        binding.btnSubmitRegister.setOnClickListener {
            sharedPref = SharedPref(requireContext())

            val username = binding.etMasukkanUsernameRegister.text.toString().trim()
            val email = binding.etMasukkanEmailRegister.text.toString().trim()
            val password = binding.etMasukkanPasswordRegister.text.toString().trim()

            if (email == user.userEmail){
                Toast.makeText(requireContext(), "Akun dengan email ${user.userEmail} telah ada!", Toast.LENGTH_SHORT).show()
            }
            if (username.isEmpty() && email.isEmpty() && password.isEmpty()){
                Toast.makeText(requireContext(), "Silahkan isi field terlebih dahulu!", Toast.LENGTH_SHORT).show()
            }
            if (checkConfirmPsw()){
                saveToPref(email, username, password)
                Toast.makeText(requireContext(), "Berhasil Membuat Akun", Toast.LENGTH_SHORT).show()
                findNavController().popBackStack()
            } else {
                Toast.makeText(requireContext(), "Password yg Anda masukkan tidak sama!" +
                        "Silakan periksa Kembali!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun saveToPref(email: String, username: String, password: String){
        val usser = User(email, username, password)
        CoroutineScope(Dispatchers.IO).launch {
            myNoteRepo.addDataUser(usser)
        }

        Toast.makeText(requireContext(), "Selamat, anda berhasil membuat akun!", Toast.LENGTH_LONG).show()
        findNavController().navigate(R.id.action_fragmentRegister_to_fragmentHome)
    }

    private fun btnToLogin(){
        binding.sudahPunyaAkun.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentRegister_to_fragmentLogin)
        }
    }
}