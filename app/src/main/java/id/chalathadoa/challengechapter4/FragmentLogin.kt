package id.chalathadoa.challengechapter4

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.chalathadoa.challengechapter4.database.User
import id.chalathadoa.challengechapter4.databinding.FragmentLoginBinding
import id.chalathadoa.challengechapter4.helper.SharedPref

class FragmentLogin : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPref
    private lateinit var user: User
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        //sharedPref = UserPref().preference
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnLogin()
        btnToRegister()
    }

    private fun btnLogin(){
        binding.btnSubmitLogin.setOnClickListener {
            sharedPref = SharedPref(requireContext())
            val email = binding.etMasukkanEmailLogin.text.toString().trim()
            val password = binding.etMasukkanPasswordLogin.text.toString().trim()
            if (email == user.userEmail && password == user.password){
                Toast.makeText(requireContext(), "Selamat datang ${user.username}, Anda berhasil login!!", Toast.LENGTH_LONG).show()
                findNavController().navigate(R.id.action_fragmentLogin_to_fragmentHome)
            } else {
                Toast.makeText(requireContext(), "Maaf, akun dengan email ${user.userEmail} Belum ada.\n" +
                        "Silakan buat akun anda dengan register terlebih dahulu", Toast.LENGTH_LONG).show()
            }
        }
    }
    private fun btnToRegister(){
        binding.tvBelumPunyaAkun.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentLogin_to_fragmentRegister)
        }
    }
}