package id.chalathadoa.challengechapter4.helper

import android.content.Context
import id.chalathadoa.challengechapter4.database.User

class SharedPref (context: Context){
    private val sharedPref = context.getSharedPreferences("email", Context.MODE_PRIVATE)

    fun setUser(){
        val editor = sharedPref.edit()
        editor.putString("username", "")
        editor.putString("userEmail", "")
        editor.putString("userPassword", "")
        editor.putBoolean("isLogin", true)
        editor.apply()
    }

    fun clearUser(){
        val editor = sharedPref.edit()
        editor.clear()
        editor.apply()
    }
}