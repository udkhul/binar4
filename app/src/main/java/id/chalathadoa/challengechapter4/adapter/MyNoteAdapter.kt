package id.chalathadoa.challengechapter4.adapter

import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import id.chalathadoa.challengechapter4.R
import id.chalathadoa.challengechapter4.database.Note
import id.chalathadoa.challengechapter4.database.User

class MyNoteAdapter (private val onDelete: (Note) -> Unit,
                     private val onEdit: (Note) -> Unit,
                     private val listener: MyNoteActionListener)
    : RecyclerView.Adapter<MyNoteAdapter.MyNoteViewHolder>() {

    private val diffCallBack = object : DiffUtil.ItemCallback<Note>(){
        override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallBack)

    fun updateData(notes: List<Note>?) = differ.submitList(notes)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyNoteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mynote, parent, false)
        return MyNoteViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyNoteViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class MyNoteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvId = view.findViewById<TextView>(R.id.tv_id)
        val tvTittle = view.findViewById<TextView>(R.id.tv_title)
        val tvBody = view.findViewById<TextView>(R.id.tv_note)
        val btnDelete = view.findViewById<ImageView>(R.id.btn_delete)
        val btnEdit = view.findViewById<ImageView>(R.id.btn_edit)

        fun bind(note: Note) {
            tvId.text = note.noteId.toString()
            tvTittle.text = note.title.toString()
            tvBody.text = note.body

            btnDelete.setOnClickListener {
                onDelete.invoke(note)
                listener.onDelete(note)
            }

            btnEdit.setOnClickListener {
                onEdit.invoke(note)
                listener.onEdit(note)
            }
        }
    }

    interface MyNoteActionListener {
        fun onDelete(note: Note)
        fun onEdit(note: Note)
    }
}
