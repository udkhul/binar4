package id.chalathadoa.challengechapter4.database

import androidx.room.*

@Dao
interface UserDao {
    @Query("SELECT * FROM User")
    fun getAllUser() : List<User>

    @Query("SELECT * FROM User WHERE userEmail = :email AND username = :username AND password = :password LIMIT 1")
    fun getRegisUser(email: String, username: String, password: String) : List<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long

    @Update
    fun updateUser(user: User): Int

    @Delete
    fun deleteUser(user: User): Int
}