package id.chalathadoa.challengechapter4.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [User::class, Note::class], version = 1)
abstract class MyNoteDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun myNoteDao(): MyNoteDao

    companion object{
        private var INSTANCE: MyNoteDatabase? = null

        fun getInstance(context: Context): MyNoteDatabase? {
            if (INSTANCE == null) {
                synchronized(MyNoteDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        MyNoteDatabase::class.java, "MyNote.db").build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance(){
            INSTANCE = null
        }
    }
}